package com.atlassian.upm.test;

import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;

/**
 * Does nothing
 */
public class NoOpModuleDescriptor extends AbstractModuleDescriptor<Object>
{
    @Override
    public Object getModule()
    {
        return null;
    }
}
