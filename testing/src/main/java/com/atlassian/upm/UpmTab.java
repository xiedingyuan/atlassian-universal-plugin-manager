package com.atlassian.upm;


public enum UpmTab
{
    UPGRADE_TAB("upgrade"),
    MANAGE_EXISTING_TAB("manage"),
    INSTALL_TAB("install"),
    COMPATIBILITY_TAB("compatibility"),
    AUDIT_LOG_TAB("log"),
    OSGI_TAB("osgi");

    private String name;

    UpmTab(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
}
