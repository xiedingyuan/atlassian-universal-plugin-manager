package com.atlassian.upm.pageobjects;

import com.google.common.base.Supplier;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.google.common.base.Suppliers.ofInstance;

public class UserInstalledPlugins extends ManageExistingTabSection
{
    @FindBy(id="upm-current-plugins")
    private WebElement userInstalledPlugins;

    @Override
    Supplier<WebElement> find(String key)
    {
        return WebElements.findPluginElement(key, "manage", ofInstance(userInstalledPlugins));
    }
}
