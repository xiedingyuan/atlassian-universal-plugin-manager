package com.atlassian.upm.pageobjects;

import javax.inject.Inject;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.webdriver.AtlassianWebDriver;

import com.google.common.base.Supplier;

import org.openqa.selenium.WebElement;

import static com.atlassian.upm.pageobjects.Suppliers.findElement;
import static com.atlassian.upm.pageobjects.WebElements.PLUGIN_NAME;
import static com.atlassian.upm.pageobjects.WebElements.PLUGIN_ROW;
import static com.atlassian.upm.pageobjects.WebElements.UPM_DETAILS;

public class UpgradeablePlugin
{
    private @Inject AtlassianWebDriver driver;
    private @Inject PageBinder binder;

    private final Supplier<WebElement> plugin;
    private final Supplier<WebElement> pluginRow;
    private final Supplier<WebElement> pluginName;
    private final Supplier<WebElement> pluginDetails;

    public UpgradeablePlugin(Supplier<WebElement> plugin)
    {
        this.plugin = plugin;
        pluginRow = findElement(PLUGIN_ROW, plugin);
        pluginDetails = findElement(UPM_DETAILS, plugin);
        pluginName = findElement(PLUGIN_NAME, plugin);
    }

    public UpgradeablePluginDetails openPluginDetails()
    {
        if (!driver.elementExistsAt(UPM_DETAILS, plugin.get()) || !driver.elementIsVisibleAt(UPM_DETAILS, plugin.get()))
        {
            pluginRow.get().click();
        }
        return binder.bind(UpgradeablePluginDetails.class, pluginDetails, this);
    }

    Supplier<WebElement> getWebElement()
    {
        return plugin;
    }

    public boolean isExpanded()
    {
        return plugin.get().getAttribute("class").contains("expanded");
    }

    public String getName()
    {
        return pluginName.get().getText();
    }
}
