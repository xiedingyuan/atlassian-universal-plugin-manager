package com.atlassian.upm.pageobjects;

import java.util.List;

import javax.inject.Inject;

import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.webdriver.AtlassianWebDriver;
import com.atlassian.webdriver.utils.Check;

import com.google.common.base.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.atlassian.upm.pageobjects.Waits.selected;
import static com.google.common.collect.Collections2.transform;

public class AuditLogTab
{
    @Inject
    private AtlassianWebDriver driver;

    private static final String PANEL_ID = "upm-panel-log";
    private static final By PANEL = By.id(PANEL_ID);
    private static final String TABLE_SELECTOR = "#upm-audit-log table";
    private static final By TABLE = By.cssSelector(TABLE_SELECTOR);
    private static final String TABLE_ROWS_SELECTOR = TABLE_SELECTOR + " tbody tr";
    private static final By TABLE_ROWS = By.cssSelector(TABLE_ROWS_SELECTOR);
    private static final String AUDIT_LOG_DATES_SELECTOR = TABLE_ROWS_SELECTOR + " td.date";
    private static final By AUDIT_LOG_DATES = By.cssSelector(AUDIT_LOG_DATES_SELECTOR);
    private static final String AUDIT_LOG_PROFILE_USERNAME_SELECTOR = TABLE_ROWS_SELECTOR + " td.username";
    private static final By AUDIT_LOG_PROFILE_USERNAME = By.cssSelector(AUDIT_LOG_PROFILE_USERNAME_SELECTOR);
    private static final String AUDIT_LOG_PROFILE_LINK_SELECTOR = AUDIT_LOG_PROFILE_USERNAME_SELECTOR + " a";
    private static final By AUDIT_LOG_PROFILE_LINK = By.cssSelector(AUDIT_LOG_PROFILE_LINK_SELECTOR);
    private static final String NEXT_LINK_ID = "upm-audit-log-next";
    private static final By NEXT_LINK = By.id(NEXT_LINK_ID);
    private static final String PREVIOUS_LINK_ID = "upm-audit-log-previous";
    private static final By PREVIOUS_LINK = By.id(PREVIOUS_LINK_ID);
    private static final String FIRST_LINK_ID = "upm-audit-log-first";
    private static final By FIRST_LINK = By.id(FIRST_LINK_ID);
    private static final String LAST_LINK_ID = "upm-audit-log-last";
    private static final By LAST_LINK = By.id(LAST_LINK_ID);
    private static final String REFRESH_LINK_ID = "upm-audit-log-refresh";
    private static final By REFRESH_LINK = By.id(REFRESH_LINK_ID);
    private static final String AUDIT_LOG_COUNT_ID = "upm-audit-log-count";
    private static final By AUDIT_LOG_COUNT = By.id(AUDIT_LOG_COUNT_ID);
    private static final String CONFIGURE_PURGE_POLICY_ID = "upm-log-configure-link";
    private static final By CONFIGURE_PURGE_POLICY = By.id(CONFIGURE_PURGE_POLICY_ID);
    private static final String PURGE_POLICY_FIELD_ID = "upm-log-configuration-days";
    private static final By PURGE_POLICY_FIELD = By.id(PURGE_POLICY_FIELD_ID);
    private static final String PURGE_POLICY_CONFIRM_ID = "upm-log-configuration-submit";
    private static final By PURGE_POLICY_CONFIRM = By.id(PURGE_POLICY_CONFIRM_ID);
    private static final String ERROR_MESSAGE_SELECTOR = "div.upm-message.error";
    private static final By ERROR_MESSAGE = By.cssSelector(ERROR_MESSAGE_SELECTOR);
    private static final String PURGE_POLICY_TEXT_ID = "upm-log-policy";
    private static final By PURGE_POLICY_TEXT = By.id(PURGE_POLICY_TEXT_ID);

    @FindBy(id = "upm-panel-log")
    private WebElement auditLogPanel;

    @WaitUntil
    public void waitUntilTabIsLoaded()
    {
        driver.waitUntil(selected(auditLogPanel));
    }

    public int getTableRowCount()
    {
        return driver.findElements(TABLE_ROWS).size();
    }

    public WebElement getAuditLogTable()
    {
        return driver.findElement(TABLE);
    }

    public void goToNextPage()
    {
        clickNavigationLink(NEXT_LINK);
    }

    public void goToPreviousPage()
    {
        clickNavigationLink(PREVIOUS_LINK);
    }

    public void goToFirstPage()
    {
        clickNavigationLink(FIRST_LINK);
    }

    public void goToLastPage()
    {
        clickNavigationLink(LAST_LINK);
    }

    public void refreshLog()
    {
        clickNavigationLink(REFRESH_LINK);
    }

    public void openPurgePolicy()
    {
        driver.findElement(CONFIGURE_PURGE_POLICY).click();
        driver.waitUntilElementIsVisible(PURGE_POLICY_FIELD);
    }

    public boolean isPurgePolicyFormOpen()
    {
        return isPurgePolicyFormOpen(driver);
    }

    public static boolean isPurgePolicyFormOpen(WebDriver driver)
    {
        return Check.elementIsVisible(PURGE_POLICY_FIELD, driver);
    }

    public void enterPurgePolicy(String days)
    {
        if (!isPurgePolicyFormOpen())
        {
            openPurgePolicy();
        }

        driver.findElement(PURGE_POLICY_FIELD).sendKeys(days);
        driver.findElement(PURGE_POLICY_CONFIRM).click();
        driver.waitUntil(new Function<WebDriver, Boolean>()
        {
            public Boolean apply(WebDriver from)
            {
                return !isPurgePolicyFormOpen(from) || isErrorMessageVisible(from);
            }
        });
    }

    public String getCurrentPurgePolicyDays()
    {
        if (!isPurgePolicyFormOpen())
        {
            openPurgePolicy();
        }

        return driver.findElement(PURGE_POLICY_FIELD).getAttribute("value");
    }

    private void clickNavigationLink(By link)
    {
        driver.findElement(link).click();
        driver.waitUntil(new Function<WebDriver, Boolean>()
        {
            public Boolean apply(WebDriver from)
            {
                return !from.findElement(PANEL).getAttribute("class").contains("loading");
            }
        });
    }

    public String getAuditLogCountText()
    {
        return driver.findElement(AUDIT_LOG_COUNT).getText();
    }

    public List<WebElement> getAuditLogEntryDates()
    {
        return driver.findElements(AUDIT_LOG_DATES);
    }

    public static boolean isErrorMessageVisible(WebDriver driver)
    {
        return Check.elementIsVisible(ERROR_MESSAGE, driver);
    }

    public String getMessageText()
    {
        return driver.findElement(ERROR_MESSAGE).getText();
    }

    public String getPurgePolicyText()
    {
        return driver.findElement(PURGE_POLICY_TEXT).getText();
    }

    public String getFirstAuditLogProfileLink()
    {
        return driver.findElement(AUDIT_LOG_PROFILE_LINK).getAttribute("href");
    }

    public Iterable<String> getAuthors()
    {
        return transform(driver.findElements(AUDIT_LOG_PROFILE_USERNAME), new Function<WebElement, String>()
        {
            public String apply(WebElement from)
            {
                return from.getText();
            }
        });
    }
}