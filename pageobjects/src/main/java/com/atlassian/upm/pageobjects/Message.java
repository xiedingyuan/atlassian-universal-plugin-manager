package com.atlassian.upm.pageobjects;

import javax.inject.Inject;

import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.webdriver.AtlassianWebDriver;

import com.google.common.base.Supplier;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.atlassian.upm.pageobjects.Suppliers.findElement;
import static com.atlassian.upm.pageobjects.Waits.and;
import static com.atlassian.upm.pageobjects.Waits.elementIsLocated;
import static com.atlassian.upm.pageobjects.Waits.elementIsVisible;

public class Message
{
    private static final By MESSAGE_TEXT = By.className("upm-message-text");

    private @Inject AtlassianWebDriver driver;
    
    private final By selector;
    private final Supplier<WebElement> container;
    private final Supplier<WebElement> message;
    private final Supplier<WebElement> messageText;

    public Message(Supplier<WebElement> container, By selector)
    {
        this.container = container;
        this.selector = selector;
        message = findElement(selector, container);
        messageText = findElement(MESSAGE_TEXT, message);
    }
    
    @WaitUntil
    public void waitUntilMessageAppears()
    {
        driver.waitUntil(and(elementIsLocated(selector, container), elementIsVisible(selector, container)));
    }

    public boolean isInfo()
    {
        return hasClass("info");
    }

    public boolean isError()
    {
        return hasClass("error");
    }

    public boolean isSuccess()
    {
        return hasClass("success");
    }
    
    public String getText()
    {
        return messageText.get().getText();
    }

    private boolean hasClass(String name)
    {
        return message.get().getAttribute("class").contains(name);
    }
}
