package com.atlassian.upm.pageobjects;

import javax.inject.Inject;

import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.webdriver.AtlassianWebDriver;

import com.google.common.base.Supplier;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.google.common.base.Suppliers.ofInstance;

public class SystemPlugins extends ManageExistingTabSection
{
    private @Inject AtlassianWebDriver driver;

    @FindBy(id="upm-system-plugins")
    private WebElement systemPlugins;

    @WaitUntil
    public void waitUntilSystemPluginsAreShown()
    {
        driver.waitUntilElementIsVisible(By.id("upm-system-plugins"));
    }

    @Override
    Supplier<WebElement> find(String key)
    {
        return WebElements.findPluginElement(key, "manage", ofInstance(systemPlugins));
    }
}
