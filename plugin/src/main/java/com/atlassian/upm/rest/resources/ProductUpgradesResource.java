package com.atlassian.upm.rest.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.atlassian.plugins.domain.model.product.Product;
import com.atlassian.upm.pac.PacClient;
import com.atlassian.upm.rest.representations.RepresentationFactory;
import com.atlassian.upm.rest.resources.permission.PermissionEnforcer;

import static com.atlassian.upm.rest.MediaTypes.PRODUCT_UPGRADES_JSON;
import static com.atlassian.upm.spi.Permission.GET_PRODUCT_UPGRADE_COMPATIBILITY;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Provides list of product versions newer than the current one.
 */
@Path("/product-upgrades")
public class ProductUpgradesResource
{
    private final RepresentationFactory representationFactory;
    private final PacClient client;
    private final PermissionEnforcer permissionEnforcer;

    public ProductUpgradesResource(RepresentationFactory factory, PacClient client, PermissionEnforcer permissionEnforcer)
    {
        this.permissionEnforcer = checkNotNull(permissionEnforcer, "permissionEnforcer");
        this.representationFactory = checkNotNull(factory, "representationFactory");
        this.client = checkNotNull(client, "client");
    }

    @GET
    @Produces(PRODUCT_UPGRADES_JSON)
    public Response get()
    {
        permissionEnforcer.enforcePermission(GET_PRODUCT_UPGRADE_COMPATIBILITY);
        Iterable<Product> productVersions = client.getProductUpgrades();
        return Response.ok(representationFactory.createProductUpgradesRepresentation(productVersions)).build();
    }
}
