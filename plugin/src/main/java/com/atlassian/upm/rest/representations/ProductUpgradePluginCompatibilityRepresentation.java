package com.atlassian.upm.rest.representations;

import java.net.URI;
import java.util.Collection;
import java.util.Map;

import javax.annotation.Nullable;

import com.atlassian.upm.PluginAccessorAndController;
import com.atlassian.upm.ProductUpgradePluginCompatibility;
import com.atlassian.upm.rest.UpmUriBuilder;
import com.atlassian.upm.spi.Plugin;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import static com.atlassian.upm.spi.Permission.GET_AVAILABLE_PLUGINS;
import static com.atlassian.upm.spi.Permission.GET_PRODUCT_UPGRADE_COMPATIBILITY;
import static com.atlassian.upm.spi.Permission.MANAGE_PLUGIN_ENABLEMENT;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Collections2.transform;
import static com.google.common.collect.ImmutableList.copyOf;

public class ProductUpgradePluginCompatibilityRepresentation
{
    @JsonProperty private final Map<String, URI> links;
    @JsonProperty private final Collection<PluginEntry> compatible;
    @JsonProperty private final Collection<PluginEntry> upgradeRequired;
    @JsonProperty private final Collection<PluginEntry> upgradeRequiredAfterProductUpgrade;
    @JsonProperty private final Collection<PluginEntry> incompatible;
    @JsonProperty private final Collection<PluginEntry> unknown;

    @JsonCreator
    public ProductUpgradePluginCompatibilityRepresentation(@JsonProperty("links") Map<String, URI> links,
        @JsonProperty("compatible") Collection<PluginEntry> compatible,
        @JsonProperty("upgradeRequired") Collection<PluginEntry> upgradeRequired,
        @JsonProperty("upgradeRequiredAfterProductUpgrade") Collection<PluginEntry> upgradeRequiredAfterProductUpgrade,
        @JsonProperty("incompatible") Collection<PluginEntry> incompatible,
        @JsonProperty("unknown") Collection<PluginEntry> unknown)
    {
        this.links = ImmutableMap.copyOf(links);
        this.compatible = copyOf(compatible);
        this.upgradeRequired = copyOf(upgradeRequired);
        this.upgradeRequiredAfterProductUpgrade = copyOf(upgradeRequiredAfterProductUpgrade);
        this.incompatible = copyOf(incompatible);
        this.unknown = copyOf(unknown);
    }

    public ProductUpgradePluginCompatibilityRepresentation(UpmUriBuilder uriBuilder, LinkBuilder linkBuilder, PluginAccessorAndController manager,
        ProductUpgradePluginCompatibility pluginCompatibility, Long productUpgradeBuildNumber)
    {
        Function<Plugin, PluginEntry> toEntries = new ToPluginEntriesFunction(uriBuilder, linkBuilder, manager);
        Function<Plugin, PluginEntry> toUpgradableEntries = new ToUpgradablePluginEntriesFunction(uriBuilder, linkBuilder, manager);

        this.links = linkBuilder.buildLinkForSelf(uriBuilder.buildProductUpgradePluginCompatibilityUri(productUpgradeBuildNumber))
            .put("installed", uriBuilder.buildInstalledPluginCollectionUri())
            .putIfPermitted(GET_PRODUCT_UPGRADE_COMPATIBILITY, "product-upgrades", uriBuilder.buildProductUpgradesUri())
            .build();
        this.compatible = transform(copyOf(pluginCompatibility.getCompatible()), toEntries);
        this.upgradeRequired = transform(copyOf(pluginCompatibility.getUpgradeRequired()), toUpgradableEntries);
        this.upgradeRequiredAfterProductUpgrade = transform(copyOf(pluginCompatibility.getUpgradeRequiredAfterProductUpgrade()), toUpgradableEntries);
        this.incompatible = transform(copyOf(pluginCompatibility.getIncompatible()), toEntries);
        this.unknown = transform(copyOf(pluginCompatibility.getUnknown()), toEntries);
    }

    public Map<String, URI> getLinks()
    {
        return links;
    }

    public Collection<PluginEntry> getCompatible()
    {
        return compatible;
    }

    public Collection<PluginEntry> getUpgradeRequired()
    {
        return upgradeRequired;
    }

    public Collection<PluginEntry> getUpgradeRequiredAfterProductUpgrade()
    {
        return upgradeRequiredAfterProductUpgrade;
    }

    public Collection<PluginEntry> getIncompatible()
    {
        return incompatible;
    }

    public Collection<PluginEntry> getUnknown()
    {
        return unknown;
    }

    public static final class PluginEntry
    {
        @JsonProperty private final Map<String, URI> links;
        @JsonProperty private final String name;
        @JsonProperty private final String key;
        @JsonProperty private final boolean enabled;
        @JsonProperty private final String restartState;

        @JsonCreator
        public PluginEntry(@JsonProperty("links") Map<String, URI> links,
            @JsonProperty("name") String name,
            @JsonProperty("enabled") boolean enabled,
            @JsonProperty("key") String key,
            @JsonProperty("restartState") String restartState)
        {
            this.links = ImmutableMap.copyOf(links);
            this.name = checkNotNull(name, "name");
            this.key = checkNotNull(key, "key");
            this.enabled = enabled;
            this.restartState = restartState;
        }

        public URI getSelfLink()
        {
            return links.get("self");
        }

        public URI getAvailableLink()
        {
            return links.get("available");
        }

        public URI getModifyLink()
        {
            return links.get("modify");
        }

        public String getName()
        {
            return name;
        }

        public String getKey()
        {
            return key;
        }

        public String getRestartState()
        {
            return restartState;
        }

        @Override
        public String toString()
        {
            return "PluginEntry{" +
                "links=" + links +
                ", name='" + name + '\'' +
                ", enabled=" + enabled +
                ", restartState='" + restartState + '\'' +
                '}';
        }
    }

    private static class ToPluginEntriesFunction implements Function<Plugin, PluginEntry>
    {
        protected final UpmUriBuilder uriBuilder;
        protected final LinkBuilder linkBuilder;
        protected final PluginAccessorAndController manager;

        ToPluginEntriesFunction(UpmUriBuilder uriBuilder, LinkBuilder linkBuilder, PluginAccessorAndController manager)
        {
            this.uriBuilder = uriBuilder;
            this.linkBuilder = linkBuilder;
            this.manager = manager;
        }

        public PluginEntry apply(@Nullable Plugin plugin)
        {
            return new PluginEntry(getLinks(plugin), plugin.getName(), manager.isPluginEnabled(plugin.getKey()), plugin.getKey(), RestartState.toString(manager.getRestartState(plugin)));
        }

        protected Map<String, URI> getLinks(Plugin plugin)
        {
            return linkBuilder.buildLinkForSelf(uriBuilder.buildPluginUri(plugin.getKey()))
                .putIfPermitted(MANAGE_PLUGIN_ENABLEMENT, "modify", uriBuilder.buildPluginUri(plugin.getKey()))
                .build();
        }
    }

    private static class ToUpgradablePluginEntriesFunction extends ToPluginEntriesFunction
    {
        ToUpgradablePluginEntriesFunction(UpmUriBuilder uriBuilder, LinkBuilder linkBuilder, PluginAccessorAndController manager)
        {
            super(uriBuilder, linkBuilder, manager);
        }

        @Override
        protected Map<String, URI> getLinks(Plugin plugin)
        {
            return linkBuilder.buildLinkForSelf(uriBuilder.buildPluginUri(plugin.getKey()))
                .putIfPermitted(GET_AVAILABLE_PLUGINS, "available", uriBuilder.buildAvailablePluginUri(plugin.getKey()))
                .putIfPermitted(MANAGE_PLUGIN_ENABLEMENT, "modify", uriBuilder.buildPluginUri(plugin.getKey()))
                .build();
        }
    }

}
