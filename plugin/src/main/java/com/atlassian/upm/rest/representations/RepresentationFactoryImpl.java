package com.atlassian.upm.rest.representations;

import java.util.Locale;

import javax.annotation.Nullable;

import com.atlassian.plugins.domain.model.plugin.PluginVersion;
import com.atlassian.plugins.domain.model.product.Product;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.upm.Change;
import com.atlassian.upm.PluginAccessorAndController;
import com.atlassian.upm.PluginConfiguration;
import com.atlassian.upm.PluginModuleConfiguration;
import com.atlassian.upm.ProductUpgradePluginCompatibility;
import com.atlassian.upm.osgi.Bundle;
import com.atlassian.upm.osgi.BundleAccessor;
import com.atlassian.upm.osgi.Package;
import com.atlassian.upm.osgi.PackageAccessor;
import com.atlassian.upm.osgi.Service;
import com.atlassian.upm.osgi.ServiceAccessor;
import com.atlassian.upm.osgi.rest.representations.BundleRepresentation;
import com.atlassian.upm.osgi.rest.representations.BundleSummaryRepresentation;
import com.atlassian.upm.osgi.rest.representations.CollectionRepresentation;
import com.atlassian.upm.osgi.rest.representations.PackageRepresentation;
import com.atlassian.upm.osgi.rest.representations.PackageSummaryRepresentation;
import com.atlassian.upm.osgi.rest.representations.ServiceRepresentation;
import com.atlassian.upm.osgi.rest.representations.ServiceSummaryRepresentation;
import com.atlassian.upm.pac.PacClient;
import com.atlassian.upm.rest.UpmUriBuilder;
import com.atlassian.upm.rest.resources.PacStatusResource;
import com.atlassian.upm.rest.resources.permission.PermissionEnforcer;
import com.atlassian.upm.spi.Plugin;
import com.atlassian.upm.spi.Plugin.Module;
import com.atlassian.upm.test.rest.resources.BuildNumberResource.BuildNumberRepresentation;

import org.springframework.beans.factory.annotation.Qualifier;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * This is the implementation of RepresentationFactory. It creates and returns representations. It uses {@code UpmUriBuilder}
 * so that the URIs can be generated in just one place.
 */
public class RepresentationFactoryImpl implements RepresentationFactory
{
    private final PluginAccessorAndController pluginAccessorAndController;
    private final UpmUriBuilder uriBuilder;
    private final LinkBuilder linkBuilder;
    private final BundleAccessor bundleAccessor;
    private final ServiceAccessor serviceAccessor;
    private final PackageAccessor packageAccessor;
    private final PermissionEnforcer permissionEnforcer;
    private final ApplicationProperties applicationProperties;

    public RepresentationFactoryImpl(PluginAccessorAndController pluginAccessorAndController,
        UpmUriBuilder uriBuilder,
        LinkBuilder linkBuilder,
        PacClient client,
        BundleAccessor bundleAccessor,
        ServiceAccessor serviceAccessor,
        PackageAccessor packageAccessor,
        PermissionEnforcer permissionEnforcer,
        @Qualifier("asyncTaskAwareApplicationProperties") ApplicationProperties applicationProperties)
    {
        this.pluginAccessorAndController = checkNotNull(pluginAccessorAndController, "pluginAccessorAndController");
        this.uriBuilder = checkNotNull(uriBuilder, "uriBuilder");
        this.linkBuilder = checkNotNull(linkBuilder, "linkBuilder");
        this.bundleAccessor = checkNotNull(bundleAccessor, "bundleAccessor");
        this.serviceAccessor = checkNotNull(serviceAccessor, "serviceAccessor");
        this.packageAccessor = checkNotNull(packageAccessor, "packageAccessor");
        this.permissionEnforcer = checkNotNull(permissionEnforcer, "permissionEnforcer");
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
    }

    public InstalledPluginCollectionRepresentation createInstalledPluginCollectionRepresentation(Locale locale)
    {
        return new InstalledPluginCollectionRepresentation(pluginAccessorAndController, uriBuilder, linkBuilder, locale);
    }

    public PluginRepresentation createPluginRepresentation(Plugin plugin)
    {
        return new PluginRepresentation(pluginAccessorAndController, checkNotNull(plugin, "plugin"), uriBuilder, linkBuilder, permissionEnforcer);
    }

    public PluginModuleRepresentation createPluginModuleRepresentation(Module module)
    {
        return new PluginModuleRepresentation(pluginAccessorAndController, checkNotNull(module, "module"), uriBuilder, linkBuilder);
    }

    /**
     * Creates a new error representation
     *
     * @param message the error message
     * @return A {@code ErrorRepresentation} with the error details
     */
    public ErrorRepresentation createErrorRepresentation(String message)
    {
        return new ErrorRepresentation(checkNotNull(message, "message"), null);
    }

    /**
     * Creates a new error representation
     *
     * @param message the error message
     * @return A {@code ErrorRepresentation} with the error details
     */
    public ErrorRepresentation createErrorRepresentation(String message, String subCode)
    {
        return new ErrorRepresentation(checkNotNull(message, "message"), checkNotNull(subCode, "subCode"));
    }

    /**
     * Creates a new error representation
     *
     * @param i18nKey the i18n key
     * @return A {@code ErrorRepresentation} with the error details
     */
    public ErrorRepresentation createI18nErrorRepresentation(String i18nKey)
    {
        return new ErrorRepresentation(null, checkNotNull(i18nKey, "i18nKey"));
    }

    public AvailablePluginCollectionRepresentation createInstallablePluginCollectionRepresentation(Iterable<PluginVersion> plugins)
    {
        return new AvailablePluginCollectionRepresentation(plugins, uriBuilder, pluginAccessorAndController, linkBuilder);
    }

    public PopularPluginCollectionRepresentation createPopularPluginCollectionRepresentation(Iterable<PluginVersion> plugins)
    {
        return new PopularPluginCollectionRepresentation(plugins, uriBuilder, pluginAccessorAndController, linkBuilder);
    }

    public SupportedPluginCollectionRepresentation createSupportedPluginCollectionRepresentation(Iterable<PluginVersion> plugins)
    {
        return new SupportedPluginCollectionRepresentation(plugins, uriBuilder, pluginAccessorAndController, linkBuilder);
    }

    public FeaturedPluginCollectionRepresentation createFeaturedPluginCollectionRepresentation(Iterable<PluginVersion> plugins)
    {
        return new FeaturedPluginCollectionRepresentation(plugins, uriBuilder, pluginAccessorAndController, linkBuilder);
    }

    public AvailablePluginRepresentation createAvailablePluginRepresentation(PluginVersion plugin)
    {
        return new AvailablePluginRepresentation(checkNotNull(plugin, "plugin"), uriBuilder, linkBuilder, pluginAccessorAndController);
    }

    public ProductUpgradesRepresentation createProductUpgradesRepresentation(Iterable<Product> productVersions)
    {
        return new ProductUpgradesRepresentation(uriBuilder, productVersions, pluginAccessorAndController, linkBuilder);
    }

    public ProductVersionRepresentation createProductVersionRepresentation(boolean development, boolean unknown)
    {
        return new ProductVersionRepresentation(development, unknown);
    }

    public ProductUpgradePluginCompatibilityRepresentation createProductUpgradePluginCompatibilityRepresentation(
        ProductUpgradePluginCompatibility pluginCompatibility,
        Long productUpgradeBuildNumber)
    {
        return new ProductUpgradePluginCompatibilityRepresentation(uriBuilder, linkBuilder, pluginAccessorAndController, pluginCompatibility, productUpgradeBuildNumber);
    }

    public UpgradesRepresentation createUpgradesRepresentation(Iterable<PluginVersion> upgrades)
    {
        return new UpgradesRepresentation(checkNotNull(upgrades, "upgrades"), uriBuilder, pluginAccessorAndController, linkBuilder);
    }

    public ChangesRequiringRestartRepresentation createChangesRequiringRestartRepresentation(Iterable<Change> restartChanges)
    {
        return new ChangesRequiringRestartRepresentation(restartChanges, uriBuilder, linkBuilder);
    }

    public CollectionRepresentation<BundleSummaryRepresentation> createOsgiBundleCollectionRepresentation()
    {
        return createOsgiBundleCollectionRepresentation(null);
    }

    public CollectionRepresentation<BundleSummaryRepresentation> createOsgiBundleCollectionRepresentation(@Nullable String term)
    {
        return new CollectionRepresentation<BundleSummaryRepresentation>(
            BundleSummaryRepresentation.wrapSummary(uriBuilder).fromIterable(bundleAccessor.getBundles(term)),
            pluginAccessorAndController.isSafeMode(),
            linkBuilder.buildLinksFor(uriBuilder.buildOsgiBundleCollectionUri(term)).build());
    }

    public BundleRepresentation createOsgiBundleRepresentation(Bundle bundle)
    {
        return new BundleRepresentation(checkNotNull(bundle, "bundle"), uriBuilder);
    }

    public CollectionRepresentation<ServiceSummaryRepresentation> createOsgiServiceCollectionRepresentation()
    {
        return new CollectionRepresentation<ServiceSummaryRepresentation>(
            ServiceSummaryRepresentation.wrapSummary(uriBuilder).fromIterable(serviceAccessor.getServices()),
            pluginAccessorAndController.isSafeMode(),
            linkBuilder.buildLinksFor(uriBuilder.buildOsgiServiceCollectionUri()).build());
    }

    public ServiceRepresentation createOsgiServiceRepresentation(Service service)
    {
        return new ServiceRepresentation(checkNotNull(service, "service"), uriBuilder);
    }

    public CollectionRepresentation<PackageSummaryRepresentation> createOsgiPackageCollectionRepresentation()
    {
        return new CollectionRepresentation<PackageSummaryRepresentation>(
            PackageSummaryRepresentation.wrapSummary(uriBuilder).fromIterable(packageAccessor.getPackages()),
            pluginAccessorAndController.isSafeMode(),
            linkBuilder.buildLinksFor(uriBuilder.buildOsgiPackageCollectionUri()).build());
    }

    public PackageRepresentation createOsgiPackageRepresentation(Package pkg)
    {
        return new PackageRepresentation(checkNotNull(pkg, "pkg"), uriBuilder);
    }

    public SafeModeErrorReenablingPluginRepresentation createSafeModeErrorReenablingPluginRepresentation(PluginConfiguration plugin)
    {
        return new SafeModeErrorReenablingPluginRepresentation(plugin);
    }

    public SafeModeErrorReenablingPluginModuleRepresentation createSafeModeErrorReenablingPluginModuleRepresentation(PluginConfiguration plugin, PluginModuleConfiguration module)
    {
        return new SafeModeErrorReenablingPluginModuleRepresentation(plugin, module);
    }

    public BuildNumberRepresentation createBuildNumberRepresentation(String buildNumber)
    {
        if (buildNumber == null)
        {
            buildNumber = applicationProperties.getBuildNumber();
        }

        return new BuildNumberRepresentation(buildNumber);
    }

    public PacStatusResource.PacStatusRepresentation createPacStatusRepresentation(boolean disabled, boolean reached)
    {
        return new PacStatusResource.PacStatusRepresentation(disabled, reached, uriBuilder, linkBuilder);
    }
}
