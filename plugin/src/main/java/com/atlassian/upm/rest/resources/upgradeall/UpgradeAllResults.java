package com.atlassian.upm.rest.resources.upgradeall;

import java.net.URI;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import static com.google.common.collect.ImmutableMap.copyOf;

public final class UpgradeAllResults extends UpgradeStatus
{
    @JsonProperty private final List<UpgradeSucceeded> successes;
    @JsonProperty private final List<UpgradeFailed> failures;
    @JsonProperty private final Map<String, URI> links;

    @JsonCreator
    public UpgradeAllResults(@JsonProperty("successes") List<UpgradeSucceeded> successes,
        @JsonProperty("failures") List<UpgradeFailed> failures,
        @JsonProperty("links") Map<String, URI> links)
    {
        super(State.COMPLETE);
        this.successes = successes;
        this.failures = failures;
        this.links = copyOf(links);
    }

    public List<UpgradeSucceeded> getSuccesses()
    {
        return successes;
    }

    public List<UpgradeFailed> getFailures()
    {
        return failures;
    }

    public Map<String, URI> getLinks()
    {
        return links;
    }
}