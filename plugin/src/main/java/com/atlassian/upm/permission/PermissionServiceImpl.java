package com.atlassian.upm.permission;

import com.atlassian.sal.api.user.UserManager;
import com.atlassian.upm.spi.Permission;
import com.atlassian.upm.spi.PermissionService;
import com.atlassian.upm.spi.Plugin;

import org.springframework.beans.factory.annotation.Qualifier;

import static com.atlassian.upm.spi.Permission.MANAGE_PLUGIN_MODULE_ENABLEMENT;

/**
 * A default implementation of the {@code PermissionService} that simply checks if the specified user is a system admin.
 * If the user is a system admin, then all permissions are granted. Otherwise, no permissions are granted.
 */
public class PermissionServiceImpl implements PermissionService
{
    private final UserManager userManager;
    static final String CONFLUENCE_MACROS_HTML = "confluence.macros.html:html";
    static final String CONFLUENCE_MACROS_HTML_INCLUDE = "confluence.macros.html:html-include";

    public PermissionServiceImpl(@Qualifier("asyncTaskAwareUserManager") UserManager userManager)
    {
        this.userManager = userManager;
    }

    public boolean hasPermission(String username, Permission permission)
    {
        switch (permission)
        {
            case GET_AUDIT_LOG:
            case GET_PLUGIN_MODULES:
            case GET_SAFE_MODE:
            case GET_OSGI_STATE:
            case MANAGE_PLUGIN_ENABLEMENT:
            case MANAGE_PLUGIN_MODULE_ENABLEMENT:
            case MANAGE_SAFE_MODE:
                return userManager.isAdmin(username);
            case GET_AVAILABLE_PLUGINS:
            case GET_PRODUCT_UPGRADE_COMPATIBILITY:
            case MANAGE_PLUGIN_INSTALL:
            case MANAGE_PLUGIN_UNINSTALL:
            case MANAGE_AUDIT_LOG:
                return userManager.isSystemAdmin(username);
            default:
                throw new IllegalArgumentException("Unhandled permission: " + permission);
        }
    }

    public boolean hasPermission(String username, Permission permission, Plugin plugin)
    {
        return hasPermission(username, permission);
    }

    public boolean hasPermission(String username, Permission permission, Plugin.Module module)
    {
        if (module != null)
        {
            final String moduleCompleteKey = module.getCompleteKey();
            // UPM-862 - hack for Confluence so their html modules will not be disabled/enabled by those others than sys-admins
            // We will remove this before we go final and once Confluence have implemented this for themselves
            if (MANAGE_PLUGIN_MODULE_ENABLEMENT == permission)
            {
                if (CONFLUENCE_MACROS_HTML.equals(moduleCompleteKey) || CONFLUENCE_MACROS_HTML_INCLUDE.equals(moduleCompleteKey))
                {
                    return userManager.isSystemAdmin(username);
                }
            }
        }
        return hasPermission(username, permission);
    }
}
