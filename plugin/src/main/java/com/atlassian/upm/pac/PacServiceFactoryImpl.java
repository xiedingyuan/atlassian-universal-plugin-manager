package com.atlassian.upm.pac;

import com.atlassian.plugins.client.service.AbstractRestServiceClient;
import com.atlassian.plugins.client.service.plugin.PluginServiceClientImpl;
import com.atlassian.plugins.client.service.plugin.PluginVersionServiceClientImpl;
import com.atlassian.plugins.client.service.product.ProductServiceClientImpl;
import com.atlassian.plugins.service.plugin.PluginService;
import com.atlassian.plugins.service.plugin.PluginVersionService;
import com.atlassian.plugins.service.product.ProductService;
import com.atlassian.upm.test.rest.resources.PacBaseUrlResource;

public final class PacServiceFactoryImpl implements PacServiceFactory
{
    private static final String SYSTEM_BASE_URL = System.getProperty("pac.baseurl", "https://plugins.atlassian.com/server");
    private static final boolean LOG_REQUESTS = Boolean.valueOf(System.getProperty("pac.log.requests"));

    public String getPacBaseUrl()
    {
        // UPM-1004 - we need to be able to manipulate this string for test purposes
        return (PacBaseUrlResource.getPacBaseUrl() != null) ? PacBaseUrlResource.getPacBaseUrl() : SYSTEM_BASE_URL;
    }

    public PluginVersionService getPluginVersionService()
    {
        return configure(new PluginVersionServiceClientImpl());
    }

    public PluginService getPluginService()
    {
        return configure(new PluginServiceClientImpl());
    }

    public ProductService getProductService()
    {
        return configure(new ProductServiceClientImpl());
    }

    private <T extends AbstractRestServiceClient<?>> T configure(T client)
    {
        client.setBaseUrl(getPacBaseUrl());
        client.setLogWebResourceRequests(LOG_REQUESTS);
        return client;
    }
}
