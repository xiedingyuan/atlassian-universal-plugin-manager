package com.atlassian.upm;

import java.io.File;
import java.util.Date;

import com.atlassian.sal.api.ApplicationProperties;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A thread-safe version of {@code ApplicationProperties}.
 */
public class AsyncTaskAwareApplicationProperties implements ApplicationProperties
{
    private final ApplicationProperties applicationProperties;
    private final ThreadLocal<String> baseUrl;
    
    public AsyncTaskAwareApplicationProperties(ApplicationProperties applicationProperties)
    {
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        baseUrl = new ThreadLocal<String>();
    }

    /**
     * Sets the base url for this thread.
     * @param baseUrl the base url for this thread.
     */
    public void setBaseUrl(final String baseUrl)
    {
        this.baseUrl.set(baseUrl);
    }
    
    /**
     * Resets the thread's base url.
     */
    public void resetBaseUrl()
    {
        this.baseUrl.remove();
    }

    /**
     * Gets the thread's base url. If it has been set specifically for this thread, that one is used.
     * Otherwise, the base url from the standard {@code ApplicationProperties} is used.
     */
    public String getBaseUrl()
    {
        if (baseUrl.get() != null)
        {
            return baseUrl.get();
        }
        else
        {
            return applicationProperties.getBaseUrl();
        }
    }
    
    public String getDisplayName()
    {
        return applicationProperties.getDisplayName();
    }

    public String getVersion()
    {
        return applicationProperties.getVersion();
    }

    public Date getBuildDate()
    {
        return applicationProperties.getBuildDate();
    }

    public String getBuildNumber()
    {
        return applicationProperties.getBuildNumber();
    }

    public File getHomeDirectory()
    {
        return applicationProperties.getHomeDirectory();
    }
}
