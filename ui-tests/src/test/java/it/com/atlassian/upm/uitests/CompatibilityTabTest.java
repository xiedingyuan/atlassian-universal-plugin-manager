package it.com.atlassian.upm.uitests;

import java.io.IOException;
import java.util.List;

import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.pageobjects.page.HomePage;
import com.atlassian.pageobjects.page.LoginPage;
import com.atlassian.pageobjects.page.WebSudoPage;
import com.atlassian.upm.pageobjects.CompatibilityTab;
import com.atlassian.upm.pageobjects.PluginManager;
import com.atlassian.upm.test.RestTester;
import com.atlassian.upm.test.UpmUiTestRunner;
import com.atlassian.webdriver.pageobjects.WebDriverTester;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Ordering;
import com.google.inject.Inject;

import org.codehaus.jettison.json.JSONException;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebElement;

import static com.atlassian.upm.pageobjects.CompatibilityTab.Category.UNKNOWN_SECTION;
import static com.atlassian.upm.pageobjects.CompatibilityTab.Version.VER_4_0;
import static com.atlassian.upm.pageobjects.CompatibilityTab.Version.VER_4_1;
import static com.atlassian.upm.test.TestPlugins.CANNOT_DISABLE_MODULE;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE_REQUIRES_RESTART;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE_WITH_CONFIG;
import static com.atlassian.upm.test.TestPlugins.OBR;
import static com.atlassian.upm.test.TestPlugins.OBR_DEP;
import static com.google.common.collect.Iterables.isEmpty;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(UpmUiTestRunner.class)
public class CompatibilityTabTest
{
    @Inject private static TestedProduct<WebDriverTester> product;
    @Inject private static RestTester restTester = new RestTester();
    private static PluginManager upm;
    private static CompatibilityTab tab;

    @BeforeClass
    public static void setUp()
    {
        upm = product.visit(LoginPage.class).
            loginAsSysAdmin(WebSudoPage.class).
            confirm(PluginManager.class);
    }

    @AfterClass
    public static void tearDown()
    {
        product.visit(HomePage.class).getHeader().logout(LoginPage.class);
    }

    @Before
    public void loadTab()
    {
        reloadTab();
    }
    
    @Test
    public void assertThatAllPluginDetailsAreShownAfterExpandAllInUserInstalledSection() throws Exception
    {
        tab.waitUntilTabIsLoaded();
        tab.selectVersion(VER_4_0);
        tab.expandAllResults(UNKNOWN_SECTION);
    }
    
    @Test
    public void assertThatAllPluginDetailsAreNotShownAfterCollapseAllInUserInstalledSection() throws Exception
    {
        tab.waitUntilTabIsLoaded();
        tab.selectVersion(VER_4_0);
        tab.expandAllResults(UNKNOWN_SECTION);
        tab.collapseAllResults(UNKNOWN_SECTION);
    }

    @Test
    public void noPluginsVisible()
    {
        Iterable<String> visiblePlugins = tab.getVisiblePlugins();
        assertTrue(isEmpty(visiblePlugins));
    }
    
    @Test
    public void recentProductShowsWarningMessage() throws Exception
    {
        tab.selectVersion(VER_4_1);
        assertTrue(tab.isRecentProductWarningDisplayed());
    }
    
    @Test
    public void nonRecentProductDoesNotShowWarningMessage() throws Exception
    {
        tab.selectVersion(VER_4_0);
        assertFalse(tab.isRecentProductWarningDisplayed());
    }

    @Test
    public void versionCompatibilityTabIncludesTestPluginInCompatibleCategory() throws JSONException
    {
        restTester.installPluginAndWaitForCompletion(CANNOT_DISABLE_MODULE);
        try
        {
            tab.selectVersion(VER_4_0);
            Iterable<String> visibleCompatiblePlugins = tab.getVisibleCompatiblePlugins();
            assertThat(visibleCompatiblePlugins, hasItem(CANNOT_DISABLE_MODULE.getName()));
        }
        finally
        {
            restTester.uninstallPluginAndVerify(CANNOT_DISABLE_MODULE);
        }
    }

    @Test
    public void pluginsInProductUpgradeCategoryHaveDisableButton() throws JSONException
    {
        try
        {
            restTester.installPluginAndWaitForCompletion(INSTALLABLE);
            tab.selectVersion(VER_4_0);
            tab.expandPluginRow(INSTALLABLE.getEscapedKey());
            assertTrue(tab.pluginHasDisableButton(INSTALLABLE.getEscapedKey()));
        }
        finally
        {
            restTester.uninstallPlugin(INSTALLABLE);
        }
    }

    @Test
    public void pluginsInProductUpgradeCategoryDisableButtonWorks() throws JSONException
    {
        try
        {
            restTester.installPluginAndWaitForCompletion(INSTALLABLE);
            tab.selectVersion(VER_4_0);
            WebElement pluginRow = tab.expandPluginRow(INSTALLABLE.getEscapedKey());
            tab.clickPluginDisableButton(INSTALLABLE.getEscapedKey());
            
            assertThat(pluginRow.getAttribute("class"), containsString("disabled"));
        }
        finally
        {
            restTester.uninstallPlugin(INSTALLABLE);
        }
    }

    @Test
    public void verifyUpgradeNowButtonIsShownOnTestPluginsInProductUpgradeCategory()
        throws JSONException, IOException
    {
        try
        {
            restTester.installPluginAndWaitForCompletion(INSTALLABLE);
            restTester.setBuildNumber("2");
            tab.selectVersion(VER_4_0);
            tab.expandPluginRow(INSTALLABLE.getEscapedKey());
            assertTrue(tab.pluginHasUpgradeButton(INSTALLABLE.getEscapedKey()));
        }
        finally
        {
            restTester.resetBuildNumber();
            restTester.uninstallPluginAndVerify(INSTALLABLE);
        }
    }
    
    @Test
    public void verifyUpgradeNowButtonIsNotShownOnPluginsRequiringRestart()
        throws JSONException, IOException
    {
        try
        {
            restTester.installPluginAndWaitForCompletion(INSTALLABLE_REQUIRES_RESTART);
            restTester.setBuildNumber("2");
            tab.selectVersion(VER_4_0);
            tab.expandPluginRow(INSTALLABLE_REQUIRES_RESTART.getEscapedKey());
            assertFalse(tab.pluginHasUpgradeButton(INSTALLABLE_REQUIRES_RESTART.getEscapedKey()));
        }
        finally
        {
            restTester.resetBuildNumber();
            restTester.deleteChangeRequiringRestart(INSTALLABLE_REQUIRES_RESTART);
        }
    }

    @Test
    public void versionCompatibilityTabIncludesInstallableTestPluginInProductUpgradeCategory()
        throws JSONException, IOException
    {
        try
        {
            restTester.installPluginAndWaitForCompletion(INSTALLABLE);
            tab.selectVersion(VER_4_0);
            Iterable<String> visibleProductUpgradePlugins = tab.getVisibleProductUpgradePlugins();
            assertThat(visibleProductUpgradePlugins, hasItem(INSTALLABLE.getName()));
        }
        finally
        {
            restTester.uninstallPluginAndVerify(INSTALLABLE);
        }
    }

    @Test
    public void versionCompatibilityTabIncludesObrTestPluginAndDepsInUnknownCategory()
        throws JSONException, IOException
    {
        try
        {
            restTester.installPluginAndWaitForCompletion(OBR);
            tab.selectVersion(VER_4_0);
            Iterable<String> visibleUnknownPlugins = tab.getVisibleUnknownPlugins();
            assertThat(visibleUnknownPlugins, hasItems(OBR.getName(), OBR_DEP.getName()));
        }
        finally
        {
            restTester.uninstallPluginAndVerify(OBR);
            restTester.uninstallPluginAndVerify(OBR_DEP);
        }
    }

    @Test
    public void versionCompatibilityTabIncludesInstallableWithConfigInIncompatibleCategory()
        throws JSONException, IOException
    {
        try
        {
            restTester.installPluginAndWaitForCompletion(INSTALLABLE_WITH_CONFIG);
            tab.selectVersion(VER_4_0);
            Iterable<String> visibleIncompatiblePlugins = tab.getVisibleIncompatiblePlugins();
            assertThat(visibleIncompatiblePlugins, hasItem(INSTALLABLE_WITH_CONFIG.getName()));
        }
        finally
        {
            restTester.uninstallPluginAndVerify(INSTALLABLE_WITH_CONFIG);
        }
    }

    @Test
    public void versionCompatibilityTabSortsUnknownCategory() throws JSONException, IOException
    {
        tab.selectVersion(VER_4_0);
        List<String> visibleUnknownPlugins = ImmutableList.copyOf(tab.getVisibleUnknownPlugins());
        List<String> expectedSortedList = Ordering.natural().sortedCopy(visibleUnknownPlugins);

        assertTrue(visibleUnknownPlugins.equals(expectedSortedList));
    }

    private static void reloadTab()
    {
        upm = product.visit(PluginManager.class);
        tab = upm.openCompatibilityTab();
    }

}
