package it.com.atlassian.upm.ui;

import com.atlassian.upm.UpmTab;
import com.atlassian.upm.test.UpmUiTestBase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.upm.UpmTab.MANAGE_EXISTING_TAB;
import static com.atlassian.upm.UpmTab.UPGRADE_TAB;
import static com.google.common.collect.Iterables.isEmpty;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.everyItem;
import static org.junit.Assert.assertTrue;

public class PluginListFilterUiIntegrationTest extends UpmUiTestBase
{
    @Before
    public void closeWindows()
    {
        uiTester.closeWindows();
        uiTester.logInAs("admin");
        uiTester.goToUpmPage();
    }

    @After
    public void logout()
    {
        uiTester.logout();
    }
    
    @Test
    public void filteringOnlyShowsPluginsThatMatchFilterValueOnManageTab()
    {
        filteringOnlyShowsPluginsThatMatchFilterValue(TabChooser.MANAGE);
    }

    @Test
    public void filteringOnlyShowsPluginsThatMatchFilterValueOnUpgradeTab()
    {
        filteringOnlyShowsPluginsThatMatchFilterValue(TabChooser.UPGRADE);
    }

    public void filteringOnlyShowsPluginsThatMatchFilterValue(TabChooser tabChooser)
    {
        tabChooser.goToTab();

        uiTester.clickElementWithId("upm-manage-show-system");
        uiTester.filterPluginList(tabChooser.getTab(), "Test");

        Iterable<String> visiblePlugins = uiTester.getVisiblePlugins(tabChooser.getTab());
        assertThat(visiblePlugins, everyItem(anyOf(containsString("test"), containsString("Test"))));
    }

    @Test
    public void filteringWithTextNotContainedInListHidesAllPluginsOnManageTab()
    {
        filteringWithTextNotContainedInListHidesAllPlugins(TabChooser.MANAGE);
    }

    @Test
    public void filteringWithTextNotContainedInListHidesAllPluginsOnUpgradeTab()
    {
        filteringWithTextNotContainedInListHidesAllPlugins(TabChooser.UPGRADE);
    }

    public void filteringWithTextNotContainedInListHidesAllPlugins(TabChooser tabChooser)
    {
        tabChooser.goToTab();

        uiTester.filterPluginList(tabChooser.getTab(), "ThisIsARandomText");
        Iterable<String> visiblePlugins = uiTester.getVisiblePlugins(tabChooser.getTab());

        assertTrue(isEmpty(visiblePlugins));
    }

    enum TabChooser
    {
        UPGRADE
            {
                @Override
                void goToTab()
                {
                    uiTester.clickUpgradeTab();
                }

                @Override
                public UpmTab getTab()
                {
                    return UPGRADE_TAB;
                }
            },
        MANAGE
            {
                @Override
                void goToTab()
                {
                    uiTester.clickManageExistingTab();
                }

                @Override
                public UpmTab getTab()
                {
                    return MANAGE_EXISTING_TAB;
                }
            };

        abstract void goToTab();

        abstract UpmTab getTab();
    }
}
