package it.com.atlassian.upm.ui;

import com.atlassian.upm.test.UpmUiTestBase;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import be.roam.hue.doj.Doj;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;

public class OsgiTabUiIntegrationTest extends UpmUiTestBase
{
    @BeforeClass
    public static void login()
    {
        uiTester.logInAs("admin");
    }

    @Before
    public void goToOsgiTab()
    {
        uiTester.goToUpmPage();
        uiTester.clickOsgiTab();
    }

    @AfterClass
    public static void logout()
    {
        uiTester.logout();
    }

    @Test
    public void assertThatTestBundleImportPackageHeaderIsExpandable() throws Exception
    {
        Doj testBundle = uiTester
            .elementById("upm-osgi-bundles")
            .get("div.upm-plugin h4.upm-plugin-name")
            .withTextContaining("test-plugin-v2-not-reloadable")
            .parent()
            .parent();
        String testBundleDivId = testBundle.id();
        uiTester.expandPluginRow(testBundleDivId);

        Doj importedPackagesExpandLink = testBundle
            .get("span.upm-osgi-parsed-header-name")
            .withTextContaining("Import-Package")
            .parent();
        importedPackagesExpandLink.click();

        Doj importedPackages = importedPackagesExpandLink
            .parent()
            .parent();

        assertThat(importedPackages, allOf(
            isExpanded(),
            hasHeaderClause("com.atlassian.plugin"),
            hasHeaderClause("com.atlassian.plugin.descriptors"),
            hasHeaderClause("com.atlassian.plugin.osgi.bridge.external"),
            hasHeaderClause("com.atlassian.plugin.osgi.external")));
    }

    private static final class ExpandedMatcher extends TypeSafeDiagnosingMatcher<Doj>
    {
        protected boolean matchesSafely(Doj item, Description mismatchDescription)
        {
            if (!item.hasClass("expanded"))
            {
                mismatchDescription.appendText("is not expanded");
                return false;
            }
            return true;
        }

        public void describeTo(Description description)
        {
            description.appendText("expanded element");
        }
    }

    private static Matcher<? super Doj> isExpanded()
    {
        return new ExpandedMatcher();
    }

    private static final class HeaderClauseMatcher extends TypeSafeDiagnosingMatcher<Doj>
    {
        private final String name;

        public HeaderClauseMatcher(String name)
        {
            this.name = name;
        }

        protected boolean matchesSafely(Doj item, Description mismatchDescription)
        {
            if (item.get("div.upm-osgi-header-clause-resolved h5.upm-module-name").withTextContaining(name).isEmpty())
            {
                mismatchDescription
                    .appendText("does not contain header clause ")
                    .appendValue(name);
                return false;
            }
            return true;
        }

        public void describeTo(Description description)
        {
            description
                .appendText("element containing header clause ")
                .appendValue(name);
        }
    }

    private static Matcher<? super Doj> hasHeaderClause(String name)
    {
        return new HeaderClauseMatcher(name);
    }
}
