package it.com.atlassian.upm.rest.resources;

import org.junit.Ignore;

@Ignore
public class AvailablePlugins
{
    public static String parseKeyFromPath(String path)
    {
        // this sucks because it requires knowledge of how the uri is constructed, but i think it's better than
        // including the plugin key in the representation when it really isn't needed for anything but testing
        // that the resource is working properly
        return path.substring(path.lastIndexOf('/') + 1);
    }
}
