package it.com.atlassian.upm.rest.resources;

import java.net.URISyntaxException;
import java.util.Collection;

import com.atlassian.upm.rest.representations.ProductUpgradePluginCompatibilityRepresentation;
import com.atlassian.upm.rest.representations.ProductUpgradePluginCompatibilityRepresentation.PluginEntry;
import com.atlassian.upm.test.TestPlugins;
import com.atlassian.upm.test.UpmResourceTestBase;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;

import org.codehaus.jettison.json.JSONException;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.Test;

import static com.atlassian.upm.test.TestPlugins.CANNOT_DISABLE_MODULE;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE_WITH_CONFIG;
import static com.google.common.collect.Lists.transform;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;

public class ProductUpgradePluginCompatibilityResourceIntegrationTest extends UpmResourceTestBase
{
    @Test
    public void assertThatProductUpgradePluginCompatibilityIsCalculatedCorrectly() throws JSONException, URISyntaxException
    {
        try
        {
            // the compatibility is only checked for installed, non-bundled plugins, so we need to install plugins first
            restTester.ensureNotInstalled(INSTALLABLE, INSTALLABLE_WITH_CONFIG, CANNOT_DISABLE_MODULE);
            restTester.installPluginsAndVerify(INSTALLABLE, INSTALLABLE_WITH_CONFIG, CANNOT_DISABLE_MODULE);

            ProductUpgradePluginCompatibilityRepresentation representation = restTester.getProductUpgradePluginCompatibility(100L);

            assertThat(representation, allOf(
                hasCompatible(CANNOT_DISABLE_MODULE),
                hasUpgradeRequired(),
                hasUpgradeRequiredAfterProductUpgrade(INSTALLABLE),
                hasIncompatible(INSTALLABLE_WITH_CONFIG)
            ));
        }
        finally
        {
            // uninstall plugins we installed earlier
            restTester.ensureNotInstalled(INSTALLABLE, INSTALLABLE_WITH_CONFIG, CANNOT_DISABLE_MODULE);
        }
    }

    @Test
    public void assertThatProductUpgradePluginCompatibilityContainsModifyLinks() throws JSONException
    {
        try
        {
            // the compatibility is only checked for installed, non-bundled plugins, so we need to install plugins first
            restTester.ensureNotInstalled(INSTALLABLE, INSTALLABLE_WITH_CONFIG, CANNOT_DISABLE_MODULE);
            restTester.installPluginsAndVerify(INSTALLABLE, INSTALLABLE_WITH_CONFIG, CANNOT_DISABLE_MODULE);

            ProductUpgradePluginCompatibilityRepresentation representation = restTester.getProductUpgradePluginCompatibility(100L);
            assertThat(representation, hasEntriesWithModifyLink());

        }
        finally
        {
            // uninstall plugins we installed earlier
            restTester.ensureNotInstalled(INSTALLABLE, INSTALLABLE_WITH_CONFIG, CANNOT_DISABLE_MODULE);
        }
    }

    private Matcher<? super ProductUpgradePluginCompatibilityRepresentation> hasEntriesWithModifyLink()
    {
        return new TypeSafeDiagnosingMatcher<ProductUpgradePluginCompatibilityRepresentation>()
        {
            protected boolean matchesSafely(ProductUpgradePluginCompatibilityRepresentation rep,
                Description description)
            {
                Iterable<PluginEntry> entries = ImmutableList.<PluginEntry>builder()
                    .addAll(rep.getCompatible())
                    .addAll(rep.getIncompatible())
                    .addAll(rep.getUnknown())
                    .addAll(rep.getUpgradeRequired())
                    .addAll(rep.getUpgradeRequiredAfterProductUpgrade())
                    .build();
                for (PluginEntry entry : entries)
                {
                    if (entry.getModifyLink() == null)
                    {
                        description.appendText(entry.getKey());
                        return false;
                    }
                }
                return true;
            }

            public void describeTo(Description description)
            {
            }
        };
    }

    private Matcher<? super ProductUpgradePluginCompatibilityRepresentation> hasCompatible(TestPlugins... plugins)
    {
        return hasPluginEntriesIn(Type.COMPATIBLE, plugins);
    }

    private Matcher<? super ProductUpgradePluginCompatibilityRepresentation> hasUpgradeRequired(TestPlugins... plugins)
    {
        return hasPluginEntriesIn(Type.UPGRADE_REQUIRED, plugins);
    }

    private Matcher<? super ProductUpgradePluginCompatibilityRepresentation> hasUpgradeRequiredAfterProductUpgrade(TestPlugins... plugins)
    {
        return hasPluginEntriesIn(Type.UPGRADE_REQUIRED_AFTER_PRODUCT_UPGRADE, plugins);
    }

    private Matcher<? super ProductUpgradePluginCompatibilityRepresentation> hasIncompatible(TestPlugins... plugins)
    {
        return hasPluginEntriesIn(Type.INCOMPATIBLE, plugins);
    }

    private Matcher<? super ProductUpgradePluginCompatibilityRepresentation> hasPluginEntriesIn(final Type type, final TestPlugins... plugins)
    {
        final Matcher<Iterable<PluginEntry>> containsEntries;
        if (plugins.length == 0)
        {
            containsEntries = Matchers.emptyIterable();
        }
        else
        {
            containsEntries = contains(transform(asList(plugins), toPluginEntryMatcher));
        }
        return new TypeSafeDiagnosingMatcher<ProductUpgradePluginCompatibilityRepresentation>()
        {
            @Override
            protected boolean matchesSafely(ProductUpgradePluginCompatibilityRepresentation rep, Description mismatchDescription)
            {
                if (!containsEntries.matches(type.get(rep)))
                {
                    mismatchDescription.appendText(type.toString()).appendText(" ").appendDescriptionOf(containsEntries);
                    return false;
                }
                return true;
            }

            public void describeTo(Description description)
            {
                description.appendText(type.toString()).appendText(" ").appendDescriptionOf(containsEntries);
            }
        };
    }

    private static final Function<TestPlugins, Matcher<? super PluginEntry>> toPluginEntryMatcher = new Function<TestPlugins, Matcher<? super PluginEntry>>()
    {
        public Matcher<? super PluginEntry> apply(final TestPlugins plugin)
        {
            return new TypeSafeDiagnosingMatcher<PluginEntry>()
            {
                @Override
                protected boolean matchesSafely(PluginEntry item, Description mismatchDescription)
                {
                    if (!plugin.getName().equals(item.getName()))
                    {
                        mismatchDescription.appendValue(item.getName());
                        return false;
                    }
                    return true;
                }

                public void describeTo(Description description)
                {
                    description.appendValue(plugin.getName());
                }
            };
        }
    };

    private enum Type
    {
        COMPATIBLE
            {
                Collection<PluginEntry> get(ProductUpgradePluginCompatibilityRepresentation rep)
                {
                    return rep.getCompatible();
                }
            },
        UPGRADE_REQUIRED
            {
                Collection<PluginEntry> get(ProductUpgradePluginCompatibilityRepresentation rep)
                {
                    return rep.getUpgradeRequired();
                }
            },
        UPGRADE_REQUIRED_AFTER_PRODUCT_UPGRADE
            {
                Collection<PluginEntry> get(ProductUpgradePluginCompatibilityRepresentation rep)
                {
                    return rep.getUpgradeRequiredAfterProductUpgrade();
                }
            },
        INCOMPATIBLE
            {
                Collection<PluginEntry> get(ProductUpgradePluginCompatibilityRepresentation rep)
                {
                    return rep.getIncompatible();
                }
            };

        abstract Collection<PluginEntry> get(ProductUpgradePluginCompatibilityRepresentation rep);

        @Override
        public String toString()
        {
            return name().replace('_', ' ').toLowerCase();
        }
    }
}
