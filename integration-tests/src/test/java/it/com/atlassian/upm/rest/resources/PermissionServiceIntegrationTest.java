package it.com.atlassian.upm.rest.resources;

import java.net.URISyntaxException;

import com.atlassian.upm.test.UpmResourceTestBase;

import com.sun.jersey.api.client.ClientResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.upm.test.TestPlugins.INSTALLABLE;
import static com.atlassian.upm.test.TestPlugins.REFIMPL_SPI_PLUGIN;
import static com.atlassian.upm.test.TestPlugins.USER_INSTALLED_WITH_MODULES;
import static com.google.common.base.Preconditions.checkState;
import static javax.ws.rs.core.Response.Status.OK;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class PermissionServiceIntegrationTest extends UpmResourceTestBase
{
    @Before
    public void enableRefimplSpi() throws URISyntaxException
    {
        ClientResponse response = restTester.enablePlugin(REFIMPL_SPI_PLUGIN);
        checkState(response.getResponseStatus().equals(OK));
    }

    @After
    public void disableRefimplSpi() throws URISyntaxException
    {
        ClientResponse response = restTester.disablePlugin(REFIMPL_SPI_PLUGIN);
        checkState(response.getResponseStatus().equals(OK));
    }

    @Test
    public void enablingRefimplSpiPreventsUpgradingAll() throws URISyntaxException
    {
        ClientResponse response = restTester.upgradeAll();
        assertThat(response.getResponseStatus(), is(UNAUTHORIZED));
    }

    @Test
    public void assertCanNotEnablePluginWhenKeyFlaggedAsNotAllowed() throws Exception
    {
        try
        {
            restTester.addAllPermissions();
            checkNotInstalled(INSTALLABLE);

            // install the plugin
            ClientResponse response = restTester.installPluginAndWaitForCompletion(INSTALLABLE);

            // check that the plugin has been installed and can be retrieved
            checkState(response.getResponseStatus().equals((OK)));

            // Make this plugin not actionable because of permissions
            restTester.blacklistPluginPermissions(INSTALLABLE);

            response = restTester.disablePlugin(INSTALLABLE);

            // assert that the plugin can not be disabled because of the permission change
            assertThat(response.getResponseStatus(), is(UNAUTHORIZED));
        }
        finally
        {
            restTester.unblacklistPluginPermissions(INSTALLABLE);
            restTester.uninstallPlugin(INSTALLABLE);
            restTester.resetPermissions();
        }
    }

    @Test
    public void assertCanNotEnableModuleWhenKeyFlaggedAsNotAllowed() throws Exception
    {
        final String moduleKey = "some_module";
        try
        {
            restTester.addAllPermissions();
            checkNotInstalled(USER_INSTALLED_WITH_MODULES);

            // install the plugin
            ClientResponse response = restTester.installPluginAndWaitForCompletion(USER_INSTALLED_WITH_MODULES);

            // check that the plugin has been installed and can be retrieved
            checkState(response.getResponseStatus().equals((OK)));

            // Make this plugin not actionable because of permissions
            restTester.blacklistModulePermissions(USER_INSTALLED_WITH_MODULES, moduleKey);

            response = restTester.disablePluginModule(USER_INSTALLED_WITH_MODULES.getKey(), moduleKey);

            // assert that the plugin can not be disabled because of the permission change
            assertThat(response.getResponseStatus(), is(UNAUTHORIZED));
        }
        finally
        {
            restTester.unblacklistModulePermissions(USER_INSTALLED_WITH_MODULES, moduleKey);
            restTester.uninstallPlugin(USER_INSTALLED_WITH_MODULES);
            restTester.resetPermissions();
        }
    }
}