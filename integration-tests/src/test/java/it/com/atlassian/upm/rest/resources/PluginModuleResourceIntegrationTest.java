package it.com.atlassian.upm.rest.resources;

import java.net.URISyntaxException;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.upm.rest.representations.PluginModuleRepresentation;
import com.atlassian.upm.test.UpmResourceTestBase;

import com.sun.jersey.api.client.ClientResponse;

import org.codehaus.jettison.json.JSONException;
import org.junit.Test;

import static com.atlassian.upm.test.TestPlugins.CANNOT_DISABLE_MODULE;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE_WITH_CONFIG;
import static com.atlassian.upm.test.TestPlugins.NOT_RELOADABLE;
import static com.atlassian.upm.test.TestPlugins.USER_INSTALLED_WITH_MODULES;
import static com.atlassian.upm.test.UpmTestGroups.BAMBOO;
import static javax.ws.rs.core.Response.Status.ACCEPTED;
import static javax.ws.rs.core.Response.Status.CONFLICT;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class PluginModuleResourceIntegrationTest extends UpmResourceTestBase
{
    private static final String TEST_PLUGIN_KEY = NOT_RELOADABLE.getKey();
    private static final String TEST_PLUGIN_MODULE_KEY = "notreloadable";
    private static final String USER_INSTALLED_TEST_PLUGIN_MODULE_KEY = "some_module";

    @Test
    public void enablePluginModuleWithInvalidPluginKeyReturnsNotFound() throws URISyntaxException
    {
        ClientResponse response = restTester.enablePluginModule("nonexistent-pluginkey", TEST_PLUGIN_MODULE_KEY);
        assertThat(response.getResponseStatus(), is(NOT_FOUND));
    }

    @Test
    public void disablePluginModuleWithInvalidPluginKeyReturnsNotFound() throws URISyntaxException
    {
        ClientResponse response = restTester.disablePluginModule("nonexistent-pluginkey", TEST_PLUGIN_MODULE_KEY);
        assertThat(response.getResponseStatus(), is(NOT_FOUND));
    }

    @Test
    public void enablePluginModuleWithInvalidModuleKeyReturnsNotFound() throws URISyntaxException
    {
        ClientResponse response = restTester.enablePluginModule(TEST_PLUGIN_KEY, "nonexistent-modulekey");
        assertThat(response.getResponseStatus(), is(NOT_FOUND));
    }

    @Test
    public void disablePluginModuleWithInvalidModuleKeyReturnsNotFound() throws URISyntaxException
    {
        ClientResponse response = restTester.disablePluginModule(TEST_PLUGIN_KEY, "nonexistent-modulekey");
        assertThat(response.getResponseStatus(), is(NOT_FOUND));
    }

    @Test
    public void enablePluginModuleWithInvalidPluginAndModuleKeyReturnsNotFound() throws URISyntaxException
    {
        ClientResponse response = restTester.enablePluginModule("nonexistent-pluginkey", "nonexistent-modulekey");
        assertThat(response.getResponseStatus(), is(NOT_FOUND));
    }

    @Test
    public void disablePluginModuleWithInvalidPluginAndModuleKeyReturnsNotFound() throws URISyntaxException
    {
        ClientResponse response = restTester.disablePluginModule("nonexistent-pluginkey", "nonexistent-modulekey");
        assertThat(response.getResponseStatus(), is(NOT_FOUND));
    }

    @Test
    public void enablePluginModuleWithValidPluginAndModuleReturnsOK() throws URISyntaxException
    {
        // make sure that the test plugin module is disabled, so we can test enabling
        restTester.disablePluginModule(TEST_PLUGIN_KEY, TEST_PLUGIN_MODULE_KEY);

        ClientResponse response = restTester.enablePluginModule(TEST_PLUGIN_KEY, TEST_PLUGIN_MODULE_KEY);
        assertThat(response.getResponseStatus(), is(OK));
        PluginModuleRepresentation returnedRepresentation = response.getEntity(PluginModuleRepresentation.class);
        assertThat(returnedRepresentation.isEnabled(), is(true));
    }

    @Test
    public void disablePluginModuleWithValidPluginAndModuleReturnsOK() throws URISyntaxException
    {
        try
        {
            ClientResponse response = restTester.disablePluginModule(TEST_PLUGIN_KEY, TEST_PLUGIN_MODULE_KEY);
            assertThat(response.getResponseStatus(), is(OK));
            PluginModuleRepresentation returnedRepresentation = response.getEntity(PluginModuleRepresentation.class);
            assertThat(returnedRepresentation.isEnabled(), is(false));
        }
        finally
        {
            // re-enable plugin module, other tests expects it to be enabled
            restTester.enablePluginModule(TEST_PLUGIN_KEY, TEST_PLUGIN_MODULE_KEY);
        }
    }

    @Test
    public void disablePluginModuleOfDisabledPluginReturnsError() throws URISyntaxException, JSONException
    {
        try
        {
            restTester.installPluginAndWaitForCompletion(USER_INSTALLED_WITH_MODULES);
            restTester.disablePlugin(USER_INSTALLED_WITH_MODULES);

            ClientResponse responsePluginModule = restTester.disablePluginModule(USER_INSTALLED_WITH_MODULES.getKey(), USER_INSTALLED_TEST_PLUGIN_MODULE_KEY);
            assertThat(responsePluginModule.getResponseStatus(), is(CONFLICT));
        }
        finally
        {
            // uninstall plugin, other tests expect it to not exist
            restTester.uninstallPluginAndVerify(USER_INSTALLED_WITH_MODULES);
        }
    }

    @Test
    public void enablePluginModuleOfDisabledPluginReturnsError() throws URISyntaxException, JSONException
    {
        try
        {
            restTester.installPluginAndWaitForCompletion(USER_INSTALLED_WITH_MODULES);
            restTester.disablePlugin(USER_INSTALLED_WITH_MODULES);

            ClientResponse responsePluginModule = restTester.enablePluginModule(USER_INSTALLED_WITH_MODULES.getKey(), USER_INSTALLED_TEST_PLUGIN_MODULE_KEY);
            assertThat(responsePluginModule.getResponseStatus(), is(CONFLICT));
        }
        finally
        {
            // uninstall plugin, other tests expect it to not exist
            restTester.uninstallPluginAndVerify(USER_INSTALLED_WITH_MODULES);
        }
    }

    @Test
    public void checkNumberOfPluginModules() throws JSONException
    {
        try
        {
            ClientResponse response = restTester.installPluginAndReturnAcceptedResponse(INSTALLABLE_WITH_CONFIG.getDownloadUri(restTester.getBaseUri()));
            assertThat(response.getResponseStatus(), is(ACCEPTED));
            restTester.waitForCompletion(response, String.class);
            assertThat(restTester.getPlugin(INSTALLABLE_WITH_CONFIG).getModules().size(), is(3));

        }
        finally
        {
            restTester.uninstallPluginAndVerify(INSTALLABLE_WITH_CONFIG);

        }
    }

    @Test
    public void disablingASystemRequiredPluginModuleViaRestSucceeds() throws URISyntaxException, JSONException
    {
        try
        {
            ClientResponse response = restTester.disablePluginModule(NOT_RELOADABLE.getKey(), "blah");
            assertThat(response.getResponseStatus(), is(OK));
        }
        finally
        {
            restTester.enablePluginModule(NOT_RELOADABLE.getKey(), "blah");
        }
    }

    @Test
    public void disablingACannotDisablePluginModuleReturnsConflict() throws URISyntaxException, JSONException
    {
        try
        {
            restTester.installPluginAndWaitForCompletion(CANNOT_DISABLE_MODULE);

            ClientResponse response = restTester.disablePluginModule(CANNOT_DISABLE_MODULE.getKey(), "module-cannot-be-disabled");
            assertThat(response.getResponseStatus(), is(FORBIDDEN));
        }
        finally
        {
            restTester.uninstallPluginAndVerify(CANNOT_DISABLE_MODULE);
        }
    }
}
